import React from 'react';
import {
  Image,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';

const LoginPage = () => {
  return (
    <View style={{flex: 1, backgroundColor: '#F7F7F7'}}>
      <View
        style={{
          backgroundColor: '#FFFFFF',
          width: 320,
          height: 381,
          alignSelf: 'center',
          marginTop: 199,
        }}>
        <View
          style={{
            height: 36,
            width: 170,
            backgroundColor: '#002558',
            alignSelf: 'center',
            marginTop: -18,
            borderRadius: 100,
            justifyContent: 'center',
          }}>
          <Text
            style={{color: 'white', alignSelf: 'center', fontFamily: 'Roboto'}}>
            Digital Approval
          </Text>
        </View>
        <Image
          style={{marginTop: 27, alignSelf: 'center'}}
          source={require('../img/image_2020-11-03_161639.png')}></Image>
        <View
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: '#002558',
            width: 288,
            height: 48,
            alignSelf: 'center',
            alignItems:'center',
            marginTop: 38,
            marginBottom: 24,
          }}>
          <Image
            style={{height: 16, width: 16, marginLeft: 16, marginRight: 16}}
            source={require('../img/MaskGroup3.png')}></Image>

          <TextInput placeholder="Alamat Email" />
        </View>
        <View
          style={{
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: '#002558',
            width: 288,
            height: 48,
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{height: 16, width: 16, marginLeft: 16, marginRight: 16}}
            source={require('../img/MaskGroup2.png')}></Image>
          <TextInput placeholder="Password" />
        </View>
        <Text
          style={{
            alignSelf: 'flex-end',
            marginTop: 16,
            marginRight: 16,
            color: '#287AE5',
          }}>
          Peset Password
        </Text>
        <TouchableOpacity
          style={{
            marginTop: 2,
            backgroundColor: '#287AE5',
            width: 288,
            height: 48,
            alignSelf: 'center',
            borderRadius: 5,
          }}>
          <Text
            style={{
              color: '#FFFFFF',
              marginTop: 13,
              alignSelf: 'center',
              fontSize: 16,
            }}>
            LOGIN
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default LoginPage;
